# Tutoriels LP CISIIE

## module html avancé

Construire un tutoriel basé sur une démo sur un des thèmes listés ci-dessous.
Chaque tutoriel correspond à un des répertoires du projet.
Le tutoriel en lui-même, ou un lien, doit être décrit dans le fichier README.md du répertoire, en utilisant la syntaxe markdown.


## thèmes prévus : 
* css transition
* css gradients
* css colors
* filter effects
* multiple column layout
* blending mode
* css animation
* css transform
* @font-face & webfonts

## Utilisation du dépôt

**Ne travaillez pas directement sur ce dépôt**

Faites un fork pour créer un dépôt dont vous êtes propriétaire.

Utilisez ce nouveau dépôt : clonez-le sur votre machine. 

**Créez une branche portant le nom de votre tuto !**

Faites vos commits/push sur votre dépôt. Quand votre tutoriel est prêt, faites un pull request pour l'intégrer dans ce dépôt.


